<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Buat Account Baru</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="{{ route('register') }}" method="POST">
        @csrf
        <label>First Name:</label><br />
        <input type="text" name="first_name" id="first_name" /><br /><br />
        <label>Last Name:</label><br />
        <input type="text" name="last_name" id="last_name" /><br /><br />
        <label>Gender:</label><br />
        <input type="radio" name="gender" /> Male <br />
        <input type="radio" name="gender" /> Female <br />
        <input type="radio" name="gender" /> Other <br /><br />
        <label>Nationality:</label><br />
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select><br /><br />
        <label>Language Spoken:</label><br />
        <input type="checkbox" name="language" />Bahasa Indonesia <br />
        <input type="checkbox" name="language" />English <br />
        <input type="checkbox" name="language" />Other <br /><br />
        <label>Bio:</label><br />
        <textarea name="bio" cols="30" rows="10"></textarea><br />
        <input type="submit" value="Sign Up" />
    </form>
</body>

</html>